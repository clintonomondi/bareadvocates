<!-- Modal -->
<div class="modal fade" id="balqesa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Balqesais an Advocate of the High Court of Kenya, the founding partner of the firm and holds the position of managing partner.</p>
                <p>She is the holder of bachelor of laws degree and diploma in law from the Kenya school of law.</p>
            <p>Before founding the firm, she worked at the firm of Ibrahim and Isaack advocates,Kullow and company advocates. Balqesa has vast experience in corporate and commercial law.Her  other  area  of  specialization  includes:  civil  and  commercial  litigation,  commercial transactions, alternative dispute resolutions,
                legislative drafting, constitutional ligation and legal services consulting.</p>

            </div>
        </div>
    </div>
</div>