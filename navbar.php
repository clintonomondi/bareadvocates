<?php
$title='BareAdvocates';
if($_SERVER['SCRIPT_NAME']=="/index.php"){
    $title='BareAdvocates-Home';
}
if($_SERVER['SCRIPT_NAME']=="/contact.php"){
    $title='BareAdvocates-Contact';
}
if($_SERVER['SCRIPT_NAME']=="/about.php"){
    $title='BareAdvocates-About';
}

?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.potenzaglobalsolutions.com/html/advocatus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 08:19:28 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Advocatus - Lawyer & Attorney HTML Template" />
    <meta name="author" content="potenzaglobalsolutions.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title;?></title>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="THE FIRM BARE & BALQESA ADVOCATES"/>
    <meta property="og:image" content="/pic/logo.jpeg"/>
    <meta property="og:description"
          content="THE FIRM BARE & BALQESA ADVOCATES is a new generation law firm that is innovative, reliable, legal advance and focused on delivering of quicklegal advice and representation to its clients.The firm consists of highly qualified, dedicated, skilled and dynamic professionals who have a wealth of experience in the realm of legal practice"/>

    <!-- Favicon -->
    <link rel="ICON" href="/pic/logo.jpeg" type="image/ico" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700&amp;display=swap" rel="stylesheet">

    <!-- CSS Global Compulsory (Do not remove)-->
    <link rel="stylesheet" href="/css/font-awesome/all.min.css" />
    <link rel="stylesheet" href="/css/flaticon/flaticon.css" />
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css" />

    <!-- Page CSS Implementing Plugins (Remove the plugin CSS here if site does not use that feature)-->
    <link rel="stylesheet" href="/css/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="/css/swiper/swiper.min.css" />
    <link rel="stylesheet" href="/css/animate/animate.min.css"/>

    <!-- Template Style -->
    <link rel="stylesheet" href="/css/style.css" />
    <script src="/alert/alertify.min.js"></script>
    <link rel="stylesheet" href="/alert/css/alertify.min.css" />
    <link rel="stylesheet" href="/alert/css/themes/default.min.css" />

</head>
<body >

<!--=================================
header -->
<header class="header header-03 header-sticky">
    <div class="topbar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="d-block d-md-flex align-items-center text-center">
                        <div class="mr-3 d-inline-block">
                            <a href="tel:1-800-555-1234"><i class="fa fa-phone mr-2 fa fa-flip-horizontal text-primary"></i>+254 722227283 </a>
                        </div>
                        <div class="mr-auto d-inline-block">
                            <a href="mailto:gethelp@advocatus.com"><i class="far fa-envelope mr-2 fa-flip-horizontal text-primary"></i>info@bareadvocates.com</a>
                        </div>
                        <div class="d-inline-block">
                            <ul class="list-unstyled">
                                <li><a href="/coming-soon.php">Careers </a></li>
                                <li><a href="/coming-soon.php"> FAQ </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-static-top navbar-expand-lg" >
        <div class="container-fluid">
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse"><i class="fas fa-align-left"></i></button>
            <a class="navbar-brand" href="/index.php">
                <img class="img-fluid logo" src="/pic/logo2.png" alt="">Bare Advocates
                <img class="img-fluid logo-sticky" src="/pic/logo2.png" alt="">
            </a>
            <div class="navbar-collapse collapse justify-content-center">
                <ul class="nav navbar-nav" >
                    <li  <?php if($_SERVER['SCRIPT_NAME']=="/index.php") { ?>  class="nav-item dropdown active"   <?php   } else{ ?>class="nav-item dropdown" <?php }?>>
                        <a class="nav-link dropdown-toggle" href="/index.php" >Home<span>/</span></a>
                    </li>
                    <li class="dropdown nav-item" >
                        <a class="nav-link" href="javascript:void(0)" data-toggle="dropdown">Services<span>/</span></a>
                        <ul class="dropdown-menu megamenu dropdown-menu-lg list-unstyled">
                            <li><a class="dropdown-item" href="/services/commercial.php">Commercial Transactions</a></li>
                            <li><a class="dropdown-item" href="/services/ligitation.php">Litigation & Alternative dispute resolution</a></li>
                            <li><a class="dropdown-item" href="/services/family.php">Family and Succession Matters</a></li>
                            <li><a class="dropdown-item" href="/services/conveyancy.php">Conveyancing and Property</a></li>
                            <li><a class="dropdown-item" href="/services/reistration.php">Registration of Legal Entities</a></li>
                            <li><a class="dropdown-item" href="/services/imigration.php">Immigration</a></li>
                            <li><a class="dropdown-item" href="/services/training.php">Training</a></li>
                            <li><a class="dropdown-item" href="/services/international.php">International & local consultancies</a></li>
                            <li><a class="dropdown-item" href="/services/policy.php">Public law and policies</a></li>
                            <li><a class="dropdown-item" href="/services/auditing.php">Legal auditing</a></li>
                        </ul>
                    </li>
                    <li  <?php if($_SERVER['SCRIPT_NAME']=="/about.php") { ?>  class="nav-item dropdown active"   <?php   } else{ ?>class="nav-item dropdown" <?php }?>>
                        <a class="nav-link dropdown-toggle" href="/about.php" >About Us<span>/</span></a>
                    </li>
                    <li  <?php if($_SERVER['SCRIPT_NAME']=="/team.php") { ?>  class="nav-item dropdown active"   <?php   } else{ ?>class="nav-item dropdown" <?php }?>>
                        <a class="nav-link dropdown-toggle" href="/team.php" >Our Team<span>/</span></a>
                    </li>
                    <li  <?php if($_SERVER['SCRIPT_NAME']=="/contact.php") { ?>  class="nav-item dropdown active"   <?php   } else{ ?>class="nav-item dropdown" <?php }?>>
                        <a class="nav-link dropdown-toggle" href="/contact.php" >Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="search">
                <a class="search-btn not_click" href="javascript:void(0);"></a>
                <div class="search-box not-click">
                    <form action="#" method="get">
                        <input type="text" class="not-click form-control" name="search" placeholder="Search.." value="" autofocus>
                        <button class="search-button" type="submit"> <i class="fa fa-search not-click"></i></button>
                    </form>
                </div>
            </div>
            <div class="add-listing">
                <a href="/team.php" class="btn btn-primary btn-hover-white">Free Consultation<i class="fas fa-arrow-right pl-3"></i></a>
            </div>
        </div>
    </nav>
</header>
<!--=================================
header -->

