<!--=================================
   footer-->
<footer class="footer footer-01 bg-light space-pt">
    <div class="footer-top">
        <div class="container">
            <div class="row justify-content-center pb-6">
                <ul class="social-icons">
                    <li class="social-rss"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="social-facebook"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="social-twitter"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="social-vimeo"><a href="#"><i class="fab fa-skype"></i></a></li>
                    <li class="social-facebook"><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                    <li class="social-twitter"><a href="#"><i class="fab fa-instagram"></i></a></li>
                    <li class="social-vimeo"><a href="#"><i class="fab fa-google"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="footer-contact-info">
                    <h5>NAIROBI OFFICE</h5>
                    <p>Give yourself the power of responsibility. Remind yourself the only thing stopping you is yourself.</p>
                    <ul class="list-unstyled mb-0"  >
                        <li> <i class="fas fa-map-signs fa-fw"></i><span>NAIROBI OFFICE
POST BANK HOUSE
2ND FLOOR
P.O BOX 51759-00101
BANDA STREET
NAIROBI.
.</span> </li>
                        <li> <i class="fas fa-microphone fa-fw"></i><span>(254) 722227283</span> </li>
                        <li> <i class="fas fa-headphones fa-fw"></i><span>info@bareadvocates.com</span> </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 mt-5 mt-md-0">
                <div class="footer-contact-info">
                    <h5>GARISSA OFFICE</h5>
                    <p>Give yourself the power of responsibility. Remind yourself the only thing stopping you is yourself.</p>
                    <ul class="list-unstyled mb-0"  >
                        <li> <i class="fas fa-map-signs fa-fw"></i><span>AL WAQF QURAN HOUSE 1ST FLOOR P.O BOX 1810-70100GARISSA.</span> </li>
                        <li> <i class="fas fa-microphone fa-fw"></i><span>(254) 721 394996</span> </li>
                        <li> <i class="fas fa-headphones fa-fw"></i><span>info@bareadvocates.com</span> </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="footer-bottom mt-3 mt-lg-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center copyright text-md-left">
                    <p class="mb-0"> &copy; Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="index.php"> Bare Advocates </a> All Rights Reserved </p>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <ul class="list-unstyled d-flex justify-content-center justify-content-md-end">
                        <li class="mr-3"> <a href="/index.php">Home</a><span class="ml-3">/</span> </li>
                        <li class="mr-3"> <a href="/about.php">About Us </a> <span class="ml-3">/</span></li>
                        <li> <a href="/contact.php">Contact Us</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--=================================
footer-->

<div id="back-to-top" class="back-to-top"><i class="fas fa-angle-up"></i> </div>

<!--=================================
Java-script -->

<!-- JS Global Compulsory (Do not remove)-->
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/popper/popper.min.js"></script>
<script src="/js/bootstrap/bootstrap.min.js"></script>

<!-- Page JS Implementing Plugins (Remove the plugin script here if site does not use that feature)-->
<script src="/js/jquery.appear.js"></script>
<script src="/js/counter/jquery.countTo.js"></script>
<script src="/js/owl-carousel/owl.carousel.min.js"></script>
<script src="/js/jarallax/jarallax.min.js"></script>
<script src="/js/swiper/swiper.min.js"></script>
<script src="/js/swiperanimation/SwiperAnimation.min.js"></script>
<script src="/js/shuffle/shuffle.min.js"></script>
<script src="/js/apexcharts/apexcharts.min.js"></script>
<script src="/js/apexcharts/charts.js"></script>

<!-- Template Scripts (Do not remove)-->
<script src="/js/custom.js"></script>
<script src="/js/countdown/jquery.downCount.js"></script>
<script src="/js/api.js"></script>

</body>

<!-- Mirrored from themes.potenzaglobalsolutions.com/html/advocatus/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 08:21:55 GMT -->
</html>
