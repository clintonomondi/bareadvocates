<?php
require_once ('navbar.php')
?>
<!--=================================
banner -->
<section class="pt-0 pt-md-6 h-sm-300 h-400 d-flex align-items-center align-items-md-start bg-primary" style="background-image: url(images/bg/wave-pattern-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-white mb-3">Contact Us</h2>
                <p class="text-white">Our  firm  continues  to build  its  clientele  range  from Individuals,  Banks,  Government  agencies, NGOs, Several corporate companies..</p>
            </div>
        </div>
    </div>
</section>
<!--=================================
banner -->
<!--=================================
Address -->
<section class="space-pb position-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-5 pt-5 pt-lg-6">
                <p class="pb-4">Stop worrying about technology problems. Let us provide the support you deserve.</p>
                <div class="feature-box mt-2">
                    <div class="feature-icon">
                        <i class="flaticon-placeholder"></i>
                    </div>
                    <div class="feature-content">
                        <h6 class="feature-title">Address:</h6>
                        <p>NAIROBI OFFICE
                            POST BANK HOUSE
                            2ND FLOOR
                            P.O BOX 51759-00101
                            BANDA STREET
                            NAIROBI.
                            . </p>
                    </div>
                </div>
                <div class="feature-box mt-2">
                    <div class="feature-icon">
                        <i class="flaticon-call"></i>
                    </div>
                    <div class="feature-content">
                        <h6 class="feature-title">Phone:</h6>
                        <p>(254) 722227283</p>
                    </div>
                </div>
                <div class="feature-box mt-2">
                    <div class="feature-icon">
                        <i class="flaticon-hourglass"></i>
                    </div>
                    <div class="feature-content">
                        <h6 class="feature-title">Support:</h6>
                        <p>24 hours a day, 7 days a week</p>
                    </div>
                </div>
                <div class="feature-box mt-2">
                    <div class="feature-icon">
                        <i class="flaticon-email"></i>
                    </div>
                    <div class="feature-content">
                        <h6 class="feature-title">Email:</h6>
                        <p>info@bareadvocates.com</p>
                    </div>
                </div>
                <div class="feature-box mt-2">
                    <div class="feature-icon">
                        <i class="flaticon-connector"></i>
                    </div>
                    <div class="feature-content">
                        <h6 class="feature-title">Social:</h6>
                        <ul class="social-icons pl-0">
                            <li class="social-rss pr-4"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-facebook pr-4"><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="social-twitter pr-4"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="social-vimeo"><a href="#"><i class="fab fa-skype"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                    <form  class="contact-form px-5 py-6 bg-light position-relative mt-5 mt-md-n7" method="post" id="submitKasae" onsubmit="return false">
                    <div class="section-title">
                        <h2>Send us direct message</h2>
                        <p>For enquiry, compliment or suggestion, please fill in the form bellow and click send.</p>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" id="exampleInputName" placeholder="Name">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" name="email" id="exampleInputSName" placeholder="Email">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" name="subject" id="exampleInputPhone" placeholder="Inquiry">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group mb-2">
                                <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="4" placeholder="Message"></textarea>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="submit"  class="btn btn-primary btn-half">Submit Message<i class="fas fa-arrow-right pl-3"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--=================================
  Address -->
<!--=================================
map -->
<div class="contact-map">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <div class="map-iframe">
<!--                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1994.393417200018!2d36.82502165800036!3d-1.3028729997624324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f112f4b45b2d9%3A0x228a1bba09951347!2sHighwayy%20Mall!5e0!3m2!1sen!2ske!4v1596319138653!5m2!1sen!2ske" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>-->
<!--                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96873.54747598754!2d-74.01503722010821!3d40.64535309479206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24416947c2109%3A0x82765c7404007886!2sBrooklyn%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1582636465372!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen=""></iframe>-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.816131197148!2d36.81740731468576!3d-1.2842364990633066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f10d14cb59d7d%3A0xeac649926d1e1a6d!2sPostbank%20House%2C%20Nairobi!5e0!3m2!1sen!2ske!4v1596946256904!5m2!1sen!2ske" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!--=================================
map -->
<!--=================================
<?php
require_once ('footer.php')
?>
