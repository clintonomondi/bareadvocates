<?php
require_once ('navbar.php')
?>

    <!--=================================
    banner -->
    <section class="position-ralative">
      <div id="main-slider" class="swiper-container">
        <div class="swiper-wrapper">
          <div class="swiper-slide h-800 h-sm-400 align-items-center d-flex bg-overlay-dark-50" style="background-image: url(/pic/slider/pic4.jpg); background-position: center center;">
            <div class="swipeinner container">
              <div class="row justify-content-center text-center">
                <div class="col-xl-8 col-md-9 col-sm-10">
                  <div class="slider-1">
                    <div class="animated" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s">
<!--                      <img class="mb-4" data-swiper-animation="fadeInUp" data-duration="1.5s" data-delay="0.25s" src="pic/logo2.png" alt="image">-->
                      <h5 class="animated text-white mb-4" data-swiper-animation="fadeInUp" data-duration="1.5s" data-delay="0.25s">Law firm with tradition</h5>
                      <h2 class="animated text-white display-6 mb-5 mb-md-5" data-swiper-animation="fadeInUp" data-duration="1.5s" data-delay="0.25s">Bare & Balqesa Advocates.</h2>
                      <a href="/team.php" class="animated4 btn btn-dark mr-2" data-swiper-animation="fadeInUp" data-duration="2.5s" data-delay="0.25s">Free consultation</a>
                      <a href="/about.php" class="animated4 btn btn-white" data-swiper-animation="fadeInUp" data-duration="2.5s" data-delay="0.25s">View more</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide h-800 h-sm-400 align-items-center d-flex bg-overlay-dark-50" style="background-image: url(pic/slider/pic3.jpg); background-position: center center;">
            <div class="swipeinner container">
              <div class="row justify-content-center text-center">
                <div class="col-xl-8 col-md-9 col-sm-10">
                  <div class="slider-1">
                    <div class="animated" data-swiper-animation="fadeInUp" data-duration="1s" data-delay="0.25s">

                      <h5 class="animated text-white mb-4" data-swiper-animation="fadeInUp" data-duration="1.5s" data-delay="0.25s">Lawyers Just Different.</h5>
                      <h2 class="animated text-white display-6 mb-5 mb-md-5" data-swiper-animation="fadeInUp" data-duration="1.5s" data-delay="0.25s">Legal Knowledge. Human Wisdom.</h2>
                      <a href="/team.php" class="animated4 btn btn-dark mr-2" data-swiper-animation="fadeInUp" data-duration="2.5s" data-delay="0.25s">Free consultation</a>
                      <a href="/about.php" class="animated4 btn btn-white" data-swiper-animation="fadeInUp" data-duration="2.5s" data-delay="0.25s">View more</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Pagination -->
      <div class="swiper-button-prev btn btn-primary btn-half text-white" tabindex="0" role="button" aria-label="Previous slide"><i class="fas fa-arrow-left mr-2"></i>Prev</div>
      <div class="swiper-button-next btn btn-primary btn-half text-white btn-half-right" tabindex="0" role="button" aria-label="Next slide">Next<i class="fas fa-arrow-right ml-2"></i></div>
    </section>
    <!--=================================
    banner -->

    <!--=================================
    About -->
    <section class="about bg-light-overlay-left pt-4 pt-sm-5 bg-holder-right bg-md-none bg-overlay-right" style="background-image: url(images/about-bg-image.png);">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 mb-5">
            <div class="section-title">
              <span class="pre-title"> Introduction </span>
              <h2>THE FIRM<span class="text-primary"> BARE & BALQESA  </span> ADVOCATES</h2>
              <p>is  a  new  generation  law  firm  that  is  innovative,  reliable, legal advance and focused on delivering of quicklegal advice and representation to its clients.The firm consists of highly qualified, dedicated, skilled and dynamic
                  professionals who have a wealth of experience in the realm of legal practice..</p>
            </div>
            <a href="#" class="btn btn-white text-dark btn-half">Read More<i class="fas fa-arrow-right pl-3"></i></a>
          </div>
          <div class="col-lg-4 mb-5">
            <p class="blockquote mb-5">One of the main areas that I work on with my clients is shedding these non-supportive beliefs and replacing them with beliefs that will help them to accomplish their desires.</p>
            <div class="signature">
              <img class="img-fluid" src="images/signature.png" alt="image">
            </div>
          </div>
          <div class="col-lg-4 align-self-end"><img class="img-fluid" src="/pic/team/main.jpeg" alt="image"></div>
        </div>
      </div>
    </section>
    <!--=================================
    About -->

    <!--=================================
    Service -->
    <section class="space-ptb">
      <div class="container">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="section-title text-center">
              <span class="pre-title"> Discover Some More Areas </span>
              <h2>Explore our wide range practice areas</h2>
              <p>It is our firm’s belief that our clientswill greatly benefit from the following services.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/commercila.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Commercial Transactions</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">Today, theAfrican Continent offers opportunities in wide range of.....</div>
                    <a class="feature-btn" href="/services/commercial.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/ligitation.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Litigation & Alternative dispute resolution</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">We  have  experienced  and  successful  litigation  team  which...</div>
                    <a class="feature-btn" href="/services/ligitation.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/family.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Family and Succession Matters</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">Our  firm  also  offers  services  on  wide  range  on  F......</div>
                    <a class="feature-btn" href="/services/family.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/conveyancy.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Conveyancing and Property </h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">We  advise  on  the  sale,  purchase  and  refinancing  of....</div>
                    <a class="feature-btn" href="/services/conveyancy.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/registration.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Registration of Legal Entities</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">Our   firm   also   deals   with   registration   of....</div>
                    <a class="feature-btn" href="/services/reistration.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/imigration.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Immigration</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">Obtaining special passes, dependents’ passes, pupil’s passes, work perm.....</div>
                    <a class="feature-btn" href="/services/imigration.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-sm-0">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/training.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>Training</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">The training programoffered by BalqesaAbdi & Co. ...........</div>
                    <a class="feature-btn" href="/services/training.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="feature-box-style-2">
              <div class="feature-box-inner">
                <div class="feature-hover-bg"></div>
                <div class="feature-image"><img class="img-fluid" src="/pic/services/international.jpg" alt="Image" /></div>
                <div class="feature-content">
                  <div class="feature-title">
                    <h6>International & local consultancies</h6>
                  </div>
                  <div class="feature-description">
                    <div class="description">We advise international and local consultancies on legal.....</div>
                    <a class="feature-btn" href="/services/international.php"><i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

          <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
                  <div class="feature-box-style-2">
                      <div class="feature-box-inner">
                          <div class="feature-hover-bg"></div>
                          <div class="feature-image"><img class="img-fluid" src="/pic/slider/pic7.jpg" alt="Image" /></div>
                          <div class="feature-content">
                              <div class="feature-title">
                                  <h6>Public law and policies</h6>
                              </div>
                              <div class="feature-description">
                                  <div class="description">We are cognizant of the central city of public law....</div>
                                  <a class="feature-btn" href="/services/policy.php"><i class="fas fa-arrow-right"></i></a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
                  <div class="feature-box-style-2">
                      <div class="feature-box-inner">
                          <div class="feature-hover-bg"></div>
                          <div class="feature-image"><img class="img-fluid" src="/pic/services/auditing.jpg" alt="Image" /></div>
                          <div class="feature-content">
                              <div class="feature-title">
                                  <h6>Legal auditing</h6>
                              </div>
                              <div class="feature-description">
                                  <div class="description">This is an independent  objective and consulting....</div>
                                  <a class="feature-btn" href="/services/auditing.php"><i class="fas fa-arrow-right"></i></a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>


    </section>
    <!--=================================
    Service -->


    <!--=================================
    Testimonials -->
    <section class="client-testimonials space-pb">
      <div class="container">
        <div class="row text-center">
          <div class="col-md-8 offset-md-2">
            <div class="section-title">
              <span class="pre-title"> Client Testimonials </span>
              <h2>What people say about us</h2>
              <p>To encompass every aspect of your brand management from creation</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="owl-carousel owl-nav-bottom-center" data-nav-dots="false" data-nav-arrow="false" data-items="2" data-lg-items="2" data-md-items="2" data-sm-items="1"  data-space="30" data-autoheight="true">
              <div class="item">
                <div class="testimonial testimonial-style-1">
                  <div class="testimonial-quote-icon"><i class="flaticon-quote"></i></div>
                  <div class="testimonial-info">
                    <ul class="list-unstyled list-inline mr-4 mb-0">
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                    </ul>
                    <div class="testimonial-content"><i> Introspection is the trick. Understand what you want, why you want it and what it will do for you. This is a critical factor, and as such, is probably the most difficult step.</i></div>
                  </div>
                  <div class="testimonial-details">
                    <div class="avatar">
                      <img class="img-fluid" src="images/testimonial/author-03.jpg" alt="">
                    </div>
                    <div class="testimonial-name">
                      <h6 class="author-name">John Doe</h6>
                      <span>Civil Lawyer</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial testimonial-style-1">
                  <div class="testimonial-quote-icon"><i class="flaticon-quote"></i></div>
                  <div class="testimonial-info">
                    <ul class="list-unstyled list-inline mr-4 mb-0">
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                    </ul>
                    <div class="testimonial-content"><i>It is truly amazing the damage that we, as parents, can inflict on our children. So why do we do it? For the most part, we don’t do it intentionally or with malice.</i></div>
                  </div>
                  <div class="testimonial-details">
                    <div class="avatar">
                      <img class="img-fluid" src="images/testimonial/author-02.jpg" alt="">
                    </div>
                    <div class="testimonial-name">
                      <h6 class="author-name">Felica Queen</h6>
                      <span>Political Expert</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial testimonial-style-1">
                  <div class="testimonial-quote-icon"><i class="flaticon-quote"></i></div>
                  <div class="testimonial-info">
                    <ul class="list-unstyled list-inline mr-4 mb-0">
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1"><i class="fas fa-star"></i></li>
                    </ul>
                    <div class="testimonial-content"><i>So, there you have it; the six steps that will help you to the fabled land of achievement and success! You now have the opportunity to push ahead and reach your potential.</i></div>
                  </div>
                  <div class="testimonial-details">
                    <div class="avatar">
                      <img class="img-fluid" src="images/testimonial/author-04.jpg" alt="">
                    </div>
                    <div class="testimonial-name">
                      <h6 class="author-name">Harry Russell</h6>
                      <span>Business Lawyer</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial testimonial-style-1">
                  <div class="testimonial-quote-icon"><i class="flaticon-quote"></i></div>
                  <div class="testimonial-info">
                    <ul class="list-unstyled list-inline mr-4 mb-0">
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                      <li class="list-inline-item mr-1 text-warning"><i class="fas fa-star"></i></li>
                    </ul>
                    <div class="testimonial-content"><i>What is the exact sequence of events that will take you to where you want to be? Have a think consciously of what you need to do. Every outcome begins with the first step</i></div>
                  </div>
                  <div class="testimonial-details">
                    <div class="avatar">
                      <img class="img-fluid" src="images/testimonial/author-06.jpg" alt="">
                    </div>
                    <div class="testimonial-name">
                      <h6 class="author-name">Mellissa Doe</h6>
                      <span>Family Consultant</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=================================
    Testimonials -->

    <!--=================================
    counter -->
    <section class="space-pb">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">
            <div class="counter counter-style-01">
              <div class="counter-icon">
                <i class="flaticon-emoji"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="1790" data-speed="10000">1790</span>
                <label>Happy Clients </label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">
            <div class="counter counter-style-01">
              <div class="counter-icon">
                <i class="flaticon-trophy"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="245" data-speed="10000">245</span>
                <label>Skilled Expert</label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-md-0">
            <div class="counter counter-style-01">
              <div class="counter-icon">
                <i class="flaticon-strong"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="491" data-speed="10000">491</span>
                <label>Finished Project</label>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="counter counter-style-01">
              <div class="counter-icon">
                <i class="flaticon-shout"></i>
              </div>
              <div class="counter-content">
                <span class="timer" data-to="1090" data-speed="10000">1090</span>
                <label>Media Posts </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=================================
    counter -->





    <!--=================================
    action box -->
    <section class="mb-n6">
      <div class="container">
        <div class="action-box text-center bg-dark p-4 p-md-5">
          <h2 class="text-primary ">Are you looking for a consultation?</h2>
          <p class="text-muted">Stop worrying about technology problems. Let us provide the support you deserve.</p>
          <a href="#" class="btn btn-primary btn-half text-white">Make An Appointment<i class="fas fa-arrow-right pl-3"></i></a>
        </div>
      </div>
    </section>
    <!--=================================
    action box -->

<?php
require_once ('footer.php')
?>