
<?php
require_once ('navbar.php');

require_once ('team/balqesa.php');
require_once ('team/adbdulai.php');
require_once ('team/alderin.php');
require_once ('team/clakson.php');
require_once ('team/jamin.php');
?>

<!--=================================
banner -->
<section class="header-inner text-center bg-overlay-dark-90" style="background-image: url(/pic/team/staff.jpeg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-white">Our Team</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active"><span>Our Team</span></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--=================================
banner -->


<!--=================================
   Team -->
<section class="bg-overlay-right space-ptb" style="background-image: url(images/bg/bg-02.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 mb-5 mb-md-0">
                <div class="section-title">
                    <span class="pre-title"> Expert lawyer Staff </span>
                    <h2>Need lawyer? meet our expert & professional staff</h2>
                    <p>It is truly amazing the damage that we, as parents, can inflict on our children. So why do we do it? For the most part, we don’t do it intentionally or with malice.</p>
                </div>
                <a href="#" class="btn btn-light btn-half">Our Attorney<i class="fas fa-arrow-right pl-3"></i></a>
            </div>
            <div class="col-lg-8 col-md-6">
                <div class="owl-carousel owl-nav-bottom-center" data-nav-dots="false" data-nav-arrow="false" data-items="2" data-lg-items="2" data-md-items="2" data-sm-items="1"  data-space="30" data-autoheight="true">
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/balqesa.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">BALQESA ABDI</a></h6>
                            <span>MANAGING PARTNER</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">BALQESA ABDI</a></h6>
                                    <span>MANAGING PARTNER</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span> Bachelor of laws degree and diploma in law</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#balqesa"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/erick.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">ERICK ODIWUOR</a></h6>
                            <span>#</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">ERICK ODIWUOR</a></h6>
                                    <span>#</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span> </li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="images/team/team-01.jpg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">ALDERIN ONGWAE</a></h6>
                            <span>DIRECTOR, CONSULTING SERVICES</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">ALDERIN ONGWAE</a></h6>
                                    <span>DIRECTOR, CONSULTING SERVICES</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span> N/A</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#alderin"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/main.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">ABDULLAHI BARE</a></h6>
                            <span>administration</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">ABDULLAHI BARE</a></h6>
                                    <span>administration</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span>  Bachelor in law, Post graduate diploma in law and a diploma in leadership management.</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#abdulai"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/mercy.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">MERCY CHEBET</a></h6>
                            <span>  Secretary</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">JAMIN MWENDANDU</a></h6>
                                    <span>  Secretary</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> mercy@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span>  ##</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/iman.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">HASSANNOOR IMAN</a></h6>
                            <span>  #</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">HASSANNOOR IMAN</a></h6>
                                    <span> ##</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span>  #</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="images/team/team-04.jpg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">JAMIN MWENDANDU</a></h6>
                            <span>  corporate   and   commercia</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">JAMIN MWENDANDU</a></h6>
                                    <span>  Corporate   and   commercia</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span>  Degree in Economics, degree in law(LLB) and is an International Trade Law Master’s Degree from the University of Nairobi</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#jamin"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="/pic/team/clackson.jpeg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">CLARKSON OTIENO</a></h6>
                            <span>corporate governance</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">CLARKSON OTIENO</a></h6>
                                    <span>Corporate governance</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (254) 722227283</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> info@bareadvocates.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span>N/A</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#klason"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="team">
                        <div class="team-image"><img class="img-fluid mx-auto" src="images/team/team-05.jpg" alt=""></div>
                        <div class="team-detail">
                            <h6><a href="#">Melanie Byrd</a></h6>
                            <span>Government Lawyer</span>
                        </div>
                        <div class="team-overlay">
                            <div class="team-info">
                                <div class="team-overlay-title">
                                    <h6><a href="#">Melanie Byrd</a></h6>
                                    <span>Government Lawyer</span>
                                </div>
                                <ul class="team-list">
                                    <li><span>Call <i class="fas fa-phone-alt"></i> :</span> (123) 345-6789</li>
                                    <li><span>Email <i class="far fa-envelope"></i> :</span> support@advocatus.com</li>
                                    <li><span>Degree <i class="fas fa-graduation-cap"></i> :</span> Master of Laws</li>
                                </ul>
                                <div class="team-contant">
                                    <p>Click -> to read more.</p>
                                    <a class="team-btn" href="#" data-toggle="modal" data-target="#balqesa"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=================================
Team -->
<!--=================================
Case Studies -->
<section class="space-ptb">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="section-title text-center">
                    <span class="pre-title"> All Team and Staff </span>
<!--                    <h2>Recent Case Studies</h2>-->
<!--                    <p>Make a list of your achievements toward your long-term goal and remind yourself.</p>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="filters-group mb-2 ">
                    <button class="btn-filter active" data-group="all">All<span>/</span></button>
<!--                    <button class="btn-filter" data-group="inquiry">Inquiry<span>/</span></button>-->
<!--                    <button class="btn-filter" data-group="family">Family<span>/</span></button>-->
<!--                    <button class="btn-filter" data-group="real-estate">Real Estate<span>/</span></button>-->
<!--                    <button class="btn-filter" data-group="criminal">Criminal<span>/</span></button>-->
<!--                    <button class="btn-filter" data-group="business">Business</button>-->
                </div>
                <div class="my-shuffle-container columns-5 mb-5">
                    <!-- Case Studies-item-01 -->
                    <div class="grid-item" data-groups='["inquiry"]'>
                        <div class="case-studies-item">
                            <img class="img-fluid" src="/pic/team/balqesa.jpeg" alt="">
                            <div class="case-studies-overlay">
                                <div class="case-studies-info">
                                    <h6>
                                        <a href="#" class="case-studies-title">BALQESA ABDI</a>
                                    </h6>
                                    <div class="case-studies-category"><span><a href="#">MANAGING PARTNER</a> <a href="#"></a> <a href="#"></a></span></div>
                                    <a class="case-studies-btn" href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Case Studies-item-01 -->
                    <!-- Case Studies-item-02 -->
                    <div class="grid-item" data-groups='["family"]'>
                        <div class="case-studies-item">
                            <img class="img-fluid" src="/pic/team/erick.jpeg" alt="">
                            <div class="case-studies-overlay">
                                <div class="case-studies-info">
                                    <h6>
                                        <a href="#" class="case-studies-title">ERICK ODIWUOR</a>
                                    </h6>
                                    <div class="case-studies-category"><span><a href="#">#</a> <a href="#"></a> <a href="#"></a></span></div>
                                    <a class="case-studies-btn" href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Case Studies-item-02 -->
                    <!-- Case Studies-item-03 -->
                    <div class="grid-item" data-groups='["real-estate"]'>
                        <div class="case-studies-item">
                            <img class="img-fluid" src="/pic/team/main.jpeg" alt="">
                            <div class="case-studies-overlay">
                                <div class="case-studies-info">
                                    <h6>
                                        <a href="#" class="case-studies-title">ABDULLAHI BARE </a>
                                    </h6>
                                    <div class="case-studies-category"><span><a href="#">Advocate of High Court </a><a href="#"></a> <a href="#"></a></span></div>
                                    <a class="case-studies-btn" href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Case Studies-item-03 -->
                    <!-- Case Studies-item-04 -->
                    <div class="grid-item" data-groups='["criminal"]'>
                        <div class="case-studies-item">
                            <img class="img-fluid" src="/pic/team/mercy.jpeg" alt="">
                            <div class="case-studies-overlay">
                                <div class="case-studies-info">
                                    <h6>
                                        <a href="#" class="case-studies-title">MERCY CHEBET</a>
                                    </h6>
                                    <div class="case-studies-category"><span><a href="#">Secretary </a> <a href="#"></a> <a href="#">Business</a></span></div>
                                    <a class="case-studies-btn" href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Case Studies-item-04 -->
                    <!-- Case Studies-item-05 -->
                    <div class="grid-item senior" data-groups='["business"]'>
                        <div class="case-studies-item">
                            <img class="img-fluid" src="/pic/team/clackson.jpeg" alt="">
                            <div class="case-studies-overlay">
                                <div class="case-studies-info">
                                    <h6>
                                        <a href="#" class="case-studies-title">CLARKSON OTIENO</a>
                                    </h6>
                                    <div class="case-studies-category"><span><a href="#">Cooperate Governance </a>, <a href="#"></a>, <a href="#"></a></span></div>
                                    <a class="case-studies-btn" href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Case Studies-item-05 -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 mb-4 mb-lg-0 text-center text-lg-right">
                <p class="mb-0">You will drift aimlessly until you arrive back at the original dock</p>
            </div>
            <div class="col-lg-5 text-center text-lg-left">
                <a href="#" class="btn btn-light btn-half">Let’s get started<i class="fas fa-arrow-right pl-3"></i></a>
            </div>
        </div>
    </div>
</section>
<!--=================================
case-studies -->
<?php
require_once ('footer.php')
?>