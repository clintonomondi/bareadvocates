<?php
require_once ('../navbar.php')
?>
    <!--=================================
    header -->
    <!--=================================
    banner -->
    <section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-white">Training</h2>
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                        <li class="breadcrumb-item active"><span>Training</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
    banner -->
    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9">
                    <img class="img-fluid" src="/pic/services/training.jpg" alt="image">
                    <h3 class="mb-4 mt-4">Training</h3>
                    <p>The training programoffered by BalqesaAbdi & Co. Advocates not only extends to the management
                        of aparticular institution,but also to the staff and citizens it serves.The purpose of the training and management development program is to improve both employee capabilities and organizations performance. </p>
                    <p>The  training  of  management  and  staff  of  management  and  staff  as  based  on  professional development program designed to provide technical and general knowledge to career employees.Training of
                        citizens is also important as it is civic education program enshrined in the Constitution</p>

                </div>
                <?php
                require_once ('../services/main.php')
                ?>
            </div>
        </div>
    </section>
<?php
require_once ('../footer.php')
?>