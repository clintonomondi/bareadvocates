<?php
require_once ('../navbar.php')
?>
    <!--=================================
    header -->
    <!--=================================
    banner -->
    <section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-white">Conveyancing and Property</h2>
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                        <li class="breadcrumb-item active"><span>Conveyancing and Property</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
    banner -->
    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9">
                    <img class="img-fluid" src="/pic/services/conveyancy.jpg" alt="image">
                    <h3 class="mb-4 mt-4">Conveyancing and Property</h3>
                    <p>We  advise  on  the  sale,  purchase  and  refinancing  of  commercial  properties  including  large  office buildings,  industrial  properties,  shopping  centers,  hospitals
                        and  health  clinics,  infrastructure  and home development projects. </p>
                    <p>We  take  pride  inour  ability  to  bring  deep  technical  skills with  effectiveness bearing  in  mind  our client’sneeds and time.</p>

                </div>
                <?php
                require_once ('../services/main.php')
                ?>
            </div>
        </div>
    </section>
<?php
require_once ('../footer.php')
?>