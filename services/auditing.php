<?php
require_once ('../navbar.php')
?>
    <!--=================================
    header -->
    <!--=================================
    banner -->
    <section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-white">Legal auditing</h2>
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                        <li class="breadcrumb-item active"><span>Legal auditing</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
    banner -->
    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9">
                    <img class="img-fluid" src="/pic/services/auditing.jpg" alt="image">
                    <h3 class="mb-4 mt-4">Legal auditing</h3>
                    <p> This is an independent  objective and consulting activity designed to add value and  improve your institution and department.It  brings  a  systematic`,  disciplined  approach  to  evaluate  and  improve  the  effectiveness  of  risk management control and governance processes. This whilst ensuring compliance with both national and county legislation.Legal auditing enable identification of potential, present and prospective legal problems to dispense with any future litigations proportions, penalties and unnecessary legal costs. </p>
               <p>Our team comprehensively reviews all the laws and regulations in the client’s business review. This includes but not  limitedto the entire legal documentations agreements, review all proposed legal reforms in the area and attendant implications to the client’s business and accordingly advice the client.</p>
                </div>
                <?php
                require_once ('../services/main.php')
                ?>
            </div>
        </div>
    </section>
<?php
require_once ('../footer.php')
?>