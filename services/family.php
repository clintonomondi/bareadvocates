<?php
require_once ('../navbar.php')
?>
    <!--=================================
    header -->
    <!--=================================
    banner -->
    <section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-white">Family and Succession Matters</h2>
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                        <li class="breadcrumb-item active"><span>Family and Succession Matters</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
    banner -->
    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9">
                    <img class="img-fluid" src="/pic/services/family.jpg" alt="image">
                    <h3 class="mb-4 mt-4">Family and Succession Matters</h3>
                    <p>Our  firm  also  offers  services  on  wide  range  on  Family  Law,  Probate,  Succession,
                        Adoption, Guardianship, Custody & Maintenance, Separation & Divorce, Children Matters </p>

                </div>
                <?php
                require_once ('../services/main.php')
                ?>
            </div>
        </div>
    </section>
<?php
require_once ('../footer.php')
?>