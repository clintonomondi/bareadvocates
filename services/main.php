<div class="col-lg-4 col-xl-3  mt-5 mt-lg-0">
    <div class="blog-sidebar">
        <!-- Practice Areas -->
        <div class="widget mb-5">
            <div class="widget-title">
                <h4>Services</h4>
            </div>
            <div class="widget-practice-areas">
                <ul class="list-unstyled list-style mb-0">
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/commercial.php") { ?>  class="active"   <?php   }  ?>><a href="/services/commercial.php"><i class="fas fa-arrow-right mr-2"></i> Commercial Transactions</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/ligitation.php") { ?>  class="active"   <?php   }  ?>><a href="/services/ligitation.php"><i class="fas fa-arrow-right mr-2"></i> Litigation & Alternative dispute</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/family.php") { ?>  class="active"   <?php   }  ?>><a href="/services/family.php"><i class="fas fa-arrow-right mr-2"></i> Family and Succession Matters</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/conveyancy.php") { ?>  class="active"   <?php   }  ?>><a href="/services/conveyancy.php"><i class="fas fa-arrow-right mr-2"></i> Conveyancing and Property</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/reistration.php") { ?>  class="active"   <?php   }  ?>><a href="/services/reistration.php"><i class="fas fa-arrow-right mr-2"></i> Registration of Legal Entities</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/imigration.php") { ?>  class="active"   <?php   }  ?>><a href="/services/imigration.php"><i class="fas fa-arrow-right mr-2"></i> Immigration</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/training.php") { ?>  class="active"   <?php   }  ?>><a href="/services/training.php"><i class="fas fa-arrow-right mr-2"></i> Training</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/international.php") { ?>  class="active"   <?php   }  ?>><a href="/services/international.php"><i class="fas fa-arrow-right mr-2"></i> International & local consultancies</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/policy.php") { ?>  class="active"   <?php   }  ?>><a href="/services/policy.php"><i class="fas fa-arrow-right mr-2"></i> Public law and policies</a></li>
                    <li <?php if($_SERVER['SCRIPT_NAME']=="/services/auditing.php") { ?>  class="active"   <?php   }  ?>><a href="/services/auditing.php"><i class="fas fa-arrow-right mr-2"></i> Legal auditing</a></li>
                </ul>
            </div>
        </div>
        <!-- Practice Areas -->
        <!-- Advice -->
        <div class="widget mb-5 bg-dark py-5 px-4  text-white">
            <div class="widget-title">
                <h6 class="text-white">Contact us </h6>
            </div>
            <p class="mb-4">Please use contact bellow to reach us any time.</p>
            <h6 class="text-primary mb-4">Call: (254)  722227283</h6>
            <ul class="list list-light list-unstyled">
                <li class="mb-2"><i class="far fa-envelope mr-2 text-primary"></i> info@bareadvocates.com</li>
                <li><i class="far fa-clock mr-2 text-primary"></i> 24 hours a day, 7 days a week</li>
            </ul>
        </div>
        <!-- Advice -->
        <!-- Brochures -->

        <!-- Brochures -->
    </div>
</div>