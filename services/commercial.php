<?php
require_once ('../navbar.php')
?>
<!--=================================
header -->
<!--=================================
banner -->
<section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-white">Commercial Transactions</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                    <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                    <li class="breadcrumb-item active"><span>Commercial Transactions</span></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--=================================
banner -->
<section class="space-ptb">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <img class="img-fluid" src="/pic/services/commercila.jpg" alt="image">
                <h3 class="mb-4 mt-4">Commercial Transactions</h3>
                <p>Today, theAfrican Continent offers opportunities in wide range of investments, business expansion, commercial
                    activities in a multi-cultural background with a lot of untapped market opportunities.Our Firm offers advice and services
                    on a full range of commercial agreements across a number of industries. Our team’s approach is to ensure each agreement meets the clients’
                    needs and goals.Our  team  offers  extensive  services  in  the  financial,  insurance,  ICT,  telecommunication,  oil  and energy, insurance,
                    immigration practice, manufacturing and retail sectors of the economy. Other  areas  of  services  we  offer  includes  but  not  limited  toadvisory  in  mergers  and  acquisitions,corporate
                    restructuring, take-overs, management buy-outs, capital markets regulatory compliance as well as conducting legal and regulatory compliance audits for private, public and listed companies. We also embark on contract drafting and reviewing as well
                    as registration of Local and International Companies. </p>
                <p>We  prepare  advisory  and  legal  opinions  on  all  facets  of  labour/employment  laws  in  Kenya,  for example
                    collective  bargaining  agreements  (CBA),  unfair  and  wrongful dismissal,  workinjury benefits,  occupational  health  and  safety  management,  non-solicitation  clauses,  redundancies, employment rights, restrictive covenants, employment policies, discrimination and pensions.The Firm’s lawyers havealso worked with
                    governments, state-owned corporations and international corporations in all stages of transactions</p>

            </div>
            <?php
            require_once ('../services/main.php')
            ?>
        </div>
    </div>
</section>
<?php
require_once ('../footer.php')
?>