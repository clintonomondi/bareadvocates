<?php
require_once ('../navbar.php')
?>
    <!--=================================
    header -->
    <!--=================================
    banner -->
    <section class="header-inner d-flex align-items-center text-center bg-overlay-dark-90" style="background-image: url(/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-white">Litigation & Alternative dispute resolution</h2>
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="/index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="/index.php">Service</a></li>
                        <li class="breadcrumb-item active"><span>Litigation & Alternative dispute resolution</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
    banner -->
    <section class="space-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9">
                    <img class="img-fluid" src="/pic/services/ligitation.jpg" alt="image">
                    <h3 class="mb-4 mt-4">Litigation & Alternative dispute resolution</h3>
                    <p>We  have  experienced  and  successful  litigation  team  which  provide  comprehensive  litigation solutions.It is our firm’s belief that court is the last resort  for  dispute  resolution.Thus,  we  take  it  upon ourselves to
                        prepare adequately should our clients  matter reach the corridors of  justice, courts or tribunals.. </p>
                    <p>Equally, we  explore  alternative  measures  with  emphasis  on  negotiations,  mediation,  conciliations and
                        arbitration. Our teamhas extensive experience in dispute resolution.</p>

                </div>
                <?php
                require_once ('../services/main.php')
                ?>
            </div>
        </div>
    </section>
<?php
require_once ('../footer.php')
?>