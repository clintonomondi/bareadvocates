<?php
require_once ('navbar.php')
?>
<!--=================================
banner -->
<section class="header-inner text-center bg-overlay-dark-90" style="background-image: url(images/bg/bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-white">About Us</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active"><span>About us</span></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--=================================
banner -->
<!--=================================
About -->
<section class="space-pb bg-light overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 pt-6 mb-5">
                <div class="section-title mb-4">
                    <span class="pre-title"> Introduction </span>
                    <h5> BALQESA  ABDI  &  CO  ADVOCATE</h5>
                </div>
                <p>is  a  new  generation  law  firm  that  is  innovative,  reliable, legal advance and focused on delivering of quicklegal advice and representation to its clients.The firm consists of highly qualified, dedicated, skilled and dynamic professionals who have a wealth of experience in the realm of legal practice.The firm is also dedicated to offer pragmatic approach to legal issues to meet the ever evolving needs of our clients.We therefore tailor our service which the client can rely on.We effectively give our clients opportunity
                    to concentrate on their core business leaving the legal part for us to worry.</p>
                <p>As a firm,we offer our clients an on matched performance anda competitive pricing policy. We also seek to develop unique relationship with each of our clients because we want to understand their individual needs.</p>
<p>Our  firm  continues  to build  its  clientele  range  from Individuals,  Banks,  Government  agencies, NGOs, Several corporate companies.</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width:83%" aria-valuenow="83" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar-title">Bare advocates success percentage rate</div>
                        <span class="progress-bar-number">83%</span>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 pt-5 mt-md-5 d-none d-lg-block">
                <img src="pic/slider/pic5.jpg" alt="image">
            </div>
        </div>
        <div class="row align-items-center mt-5 mt-lg-n5">
            <div class="col-md-4">
                <div class="bg-dark p-5">
                    <h5 class="text-primary pb-3">
                       Our Mission
                    </h5>
                    <p class="mb-0 text-white">TO SKILLFULLY  </p>
                    <p class="mb-0 text-white"> PROVIDE EFFECTIVE AND EFFICIENT LEGAL  </p>
                    <p class="mb-0 text-white"> SOLUTIONS  .</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-primary p-5">
                    <h5 class="text-dark pb-3">
                        Our Vision
                    </h5>
                    <p class="mb-0 text-white">TO BE A PROMINENT LAW FIRM RECOGNIZED FOR QUALITY LEGAL SERVICES AND DEDICATION TO OUR CLIENTS AND THEIR FUTURE</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-white p-5">
                    <h5 class="text-primary pb-3">
                        Our Core Values
                    </h5>
                  <ul>
                      <li>Integrity</li>
                      <li>Accountability</li>
                      <li>Transparency</li>
                      <li>Professionalism</li>
                      <li>Result oriented</li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=================================
About -->
<!--=================================


<?php
require_once ('footer.php')
?>
