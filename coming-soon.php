
<?php
require_once ('navbar.php')
?>
<!--=================================
banner -->
<section class="text-center d-flex align-items-center bg-overlay-dark-50 vh-100 coming-soon" style="background-image: url(/pic/slider/pic4.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="/index.php">
                    <img class="img-fluid" src="/pic/logo2.png" alt="image">
                </a>
                <div class="section-title text-center mt-5">
                    <h2 class="text-white title-effect">We Are Coming Very Soon!</h2>
                    <p class="text-white">Subscribe to Our Newsletter. Provide your email address & we will notify you when site is ready.</p>
                </div>
            </div>
            <div class="col-md-6 offset-md-3 mb-5">
                <div class="newsletter">
                    <form class="d-flex">
                        <div class="form-group mb-0">
                            <input type="email" class="form-control" placeholder="Your e-mail">
                        </div>
                        <button type="submit" class="btn btn-primary">Notify Me</button>
                    </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="countdown mr-md-4 mr-3 mb-md-0 mb-3">
                    <span class="days">00</span>
                    <p class="days_ref">Dys</p>
                </div>
                <div class="countdown mr-md-4 mr-3 mb-md-0 mb-3">
                    <span class="hours">00</span>
                    <p class="hours_ref">Hrs</p>
                </div>
                <div class="countdown mr-md-4 mr-3 mb-md-0 mb-3">
                    <span class="minutes">00</span>
                    <p class="minutes_ref">Min</p>
                </div>
                <div class="countdown mr-0">
                    <span class="seconds">00</span>
                    <p class="seconds_ref">Sec</p>
                </div>
            </div>
            <div class="col-md-12">
                <ul class="social-icons pl-0 mt-5 position-relative">
                    <li class="social-rss pr-4"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="social-facebook pr-4"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="social-twitter pr-4"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="social-vimeo"><a href="#"><i class="fab fa-skype"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--=================================

<?php
require_once ('footer.php')
?>